// debugger
var x = getRandom();
var y = getRandom();
var suma = x + y;

setCaptcha('.captchaX', x)
setCaptcha('.captchaY', y)

var selector = '.captchaZ'
var elemz = document.querySelector(selector)


function getRandom() {
    return Math.round(Math.random() * 100)
}

function setCaptcha(selector, value) {
    var elem = document.querySelector(selector)
    elem.disabled = true;
    elem.value = value;
}

function updateClock() {
    var clock = document.querySelector('.clock');
    var date = new Date();
    var time = date.toLocaleTimeString();
    clock.innerText = time;
}
setInterval(updateClock, 1000)


var captchaMessage = document.querySelector('.captcha-message')
var captchaZInput = document.querySelector('.captchaZ')

captchaZInput.addEventListener('keyup', checkCaptcha)

function checkCaptcha() {
    var value = captchaZInput.value

    if (value == suma) {
        captchaMessage.innerText = 'Captcha OK';
        captchaMessage.style = "color: green"
    } else {
        captchaMessage.innerText = 'Bad Captcha';
        captchaMessage.style = "color: red"
    }
}

var sendButton = document.querySelector('.button-send');

sendButton.addEventListener('click', function(){

    fetch('http://192.168.13.127:3000/messages/',{
        method:"POST",
        body: JSON.stringify(getMessage() ),
        headers:{
            'Content-Type':'application/json'
        }
    }).then(function(){
        loadMessages()
    })

})
var lastID = 3

function getMessage(){
    var usernameElem = $('#username');
    var messageElem = $('#message');

    var message = {
        // id: ++lastID,
        username: usernameElem.val(), 
        message: messageElem.val(),
    };
    return message;
}

function insertMessage(message){
    var tr = $('<tr>')
        .append('<td>' + message.id + '</td>')
        .append('<td>' + message.username + '</td>')
        .append('<td>' + message.message + '</td>')
    $('tbody').append(tr)
}

function insertMessages(messages){
    messages.forEach(insertMessage);
}

// document.querySelector('.load-messages').addEventListener('click)
$('.load-messages').on('click', loadMessages)

function loadMessages(){
    fetch('http://localhost:3000/messages/')
    .then(function(response){
        return response.json()
    })
    .then(function(messages){
        $('tbody').empty()
        insertMessages(messages)
    })
}